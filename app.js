const path = require('path')
const Telegraf = require('telegraf')
const TelegrafI18n = require('telegraf-i18n')
const { match } = require('telegraf-i18n')
const TelegrafBotanio = require('telegraf-botanio')
const Session = require('telegraf-session-redis')
const Stage = require('telegraf/stage')
const config = require('config')
const logger = require('./lib/logger')
const middleware = require('./middleware')

const session = new Session({
  store: {host: config.redisHost},
  ttl: config.sessionTTL,
  getSessionKey(ctx) {
    return ctx.from && ctx.chat
      ? `wcszombot:${ctx.from.id}:${ctx.chat.id}`
      : undefined
  }
})

const i18n = new TelegrafI18n({
  sessionName: 'session',
  defaultLanguage: config.defaultLocale,
  directory: path.resolve(__dirname, 'locales')
})

const stage = new Stage()
stage.register(middleware.check.scene)

const botanio = new TelegrafBotanio(process.env.BOTANIO_TOKEN)
const bot = new Telegraf(process.env.BOT_TOKEN)
bot.telegram.getMe().then(info => bot.options.username = info.username)

bot.catch(err => logger.error('bot:', err))
bot.use(botanio.middleware())
bot.use(session.middleware())
bot.use(i18n.middleware())
bot.use(middleware.logger)
bot.use(middleware.user)
bot.on('message', middleware.listener)
bot.use(stage.middleware())

// Public commands
bot.command('help', middleware.help)
bot.command('wcs', middleware.more)
bot.hears(match('keyboard.commands'), middleware.help.commands)
bot.hears(match('keyboard.paid'), middleware.dialog.paid)

// User verification
bot.use(middleware.check)

// Verified users only
config.resources.forEach(resource => {
  bot.command(resource.id, middleware.request(resource))
})

bot.command('start', middleware.start)
bot.command('list', middleware.list)
bot.command('donate', middleware.donate)
bot.hears(match('keyboard.list'), middleware.list)
bot.hears(match('keyboard.donate'), middleware.donate)
bot.on('message', middleware.notUnderstand)

bot.startPolling()
module.exports = bot
