const path = require('path')

/** @define config */
const config = {
  mongoPath: 'mongodb://mongo/wcszombot',
  redisHost: 'redis',
  defaultLocale: 'ru',

  log: {
    info: '/data/log/wcszombot-info.log',
    error: '/data/log/wcszombot-error.log'
  },

  resourceCoverPath: path.resolve(__dirname, '../assets/resources'),

  resources: [
    {
      id: 'theopen2017',
      cover: 'theopen2017.jpg',
      title: '🇺🇸 The Open 2017',
      description: '23–26 ноября, Лос-Анджелес, Калифорния.\nВидео конкурсов всех номинаций.',
      url: 'http://go.wcs.life/theopen2017-wcszombot',
      password: 'theo2017',
    },
    {
      id: 'usopen2016',
      cover: 'usopen2016.jpg',
      title: '🇺🇸 US Open 2016',
      description: '24–27 ноября, Лос-Анджелес, Калифорния.\nВидео конкурсов всех номинаций.',
      url: 'http://go.wcs.life/uso2016-wcszombot',
      password: 'usop2016',
    }
  ],

  questions: [
    {
      id: 'fomin',
      q: 'Как зовут Фомина?',
      a: /(олег|олеж|oleg)/i
    },
    {
      id: 'tatiana',
      q: 'Как зовут партнершу Джордана?',
      a: /(татьян|таня|тане|tatiana)/i
    },
    {
      id: 'elfest',
      q: 'В каком городе проходит ElFest?',
      a: /(казан|kazan)/i
    },
    {
      id: 'triple',
      q: 'Степ-степ…?',
      a: /(трипл|triple)/i
    },
    {
      id: 'saratov',
      q: 'Назови родной город Семёна и Маши',
      a: /(саратов|saratov)/i
    }
  ],

  dialog: {
    paid: {
      // https://t.me/addstickers/sp723d1e154cbff0bbd072910dafa5caf0_by_stckrRobot
      setName: 'sp723d1e154cbff0bbd072910dafa5caf0_by_stckrRobot'
    }
  }

}

module.exports = config
