/** @define config */
const config = {
  sessionTTL: 30*24*60*60, // 30 days
}

module.exports = config
