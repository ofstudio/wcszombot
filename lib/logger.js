const winston = require('winston')
const config = require('config')


/** @define logger */
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      level: process.env.NODE_ENV !== 'production' ? 'silly' : 'info',
      colorize: true,
      handleExceptions: false,
      json: false,
      timestamp: true
    }),
    new (winston.transports.File)({
      level: 'info',
      name: 'info',
      filename: config.log.info,
      colorize: false,
      handleExceptions: false,
      json: false,
      maxsize: 1024 * 100, // 100 KB
      maxFiles: 5,
      tailable: true
    }),
    new (winston.transports.File)({
      level: 'error',
      name: 'error',
      filename: config.log.error,
      colorize: false,
      handleExceptions: true,
      json: false,
      maxsize: 1024 * 200, // 100 KB
      maxFiles: 10,
      tailable: true
    })
  ]
})


module.exports = logger
module.exports.middleware = (msg) => {
  return (ctx, next) => {
    logger.info(msg, ctx.chat)
    return next()
  }
}
