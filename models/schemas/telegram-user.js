const mongoose = require('mongoose')
const Schema = mongoose.Schema

module.exports = new Schema({
  id: {type: Number, required: true},
  first_name: {type: String, required: true},
  last_name: {type: String},
  username: {type: String},
  language_code: {type: String},
  is_bot: {type: Boolean}
})
