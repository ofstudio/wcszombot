const mongoose = require('mongoose')
const Schema = mongoose.Schema

module.exports = new Schema({
  id: {type: Number, required: true},
  type: {type: String},
  title: {type: String},
  username: {type: String},
  first_name: {type: String},
  last_name: {type: String},
  all_members_are_administrators: {type: Boolean},
  photo: {},
  description: {type: String},
  invite_link: {type: String},
  pinned_message: {},
  sticker_set_name: {type: String},
  can_set_sticker_set: {type: Boolean}
})
