const mongoose = require('mongoose')
const telegramUserSchema = require('./schemas/telegram-user')
const Schema = mongoose.Schema

const userSchema = new Schema({
  telegram: {type: telegramUserSchema},
  counter: {
    activity: {type: Number, required: true, default: 0},
    requests: {type: Number, required: true, default: 0},
    paidButton: {type: Number, required: true, default: 0}
  }
}, {timestamps: true})

userSchema.index({'telegram.id': 1}, {unique: true})

/** @class User */
const User = mongoose.model('User', userSchema)
module.exports = User
