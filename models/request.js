const mongoose = require('mongoose')
const telegramUserSchema = require('./schemas/telegram-user')
const Schema = mongoose.Schema

const requestSchema = new Schema({
  from: {type: telegramUserSchema},
  resourceId: {type: String, required: true, index: true}
}, {timestamps: true})

requestSchema.index({'telegram.id': 1})

/** @class Request */
const Request = mongoose.model('Request', requestSchema)
module.exports = Request
