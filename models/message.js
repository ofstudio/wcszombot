const mongoose = require('mongoose')
const telegramUserSchema = require('./schemas/telegram-user')
const telegramChatSchema = require('./schemas/telegram-chat')
const Schema = mongoose.Schema

const messageSchema = new Schema({
  message_id: {type: Number, required: true},
  from: {type: telegramUserSchema, required: true},
  date: {type: Number, required: true, index: true},
  chat: {type: telegramChatSchema, required: true}
}, {strict: false, timestamps: true})

messageSchema.index({message_id: 1, 'chat.id': -1}, {unique: true})

/** @class Message */
const Message = mongoose.model('Message', messageSchema)
module.exports = Message
