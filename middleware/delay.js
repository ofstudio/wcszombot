module.exports = (t1 = 0, t2 = t1) => {
  const delay = t1 + Math.floor(Math.random() * (t2 - t1))
  return ctx => {
    ctx.replyWithChatAction('typing')
      .catch(err => ctx.logger.error('delay', err))
    return new Promise(res => setTimeout(res, delay))
  }
}
