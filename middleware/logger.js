const logger = require('../lib/logger')

function formatParams(params) {
  return params.map(p => `[${p}]`).join('')
}

function formatSrc(ctx) {
  return `id=${ctx.from.id} first_name=${ctx.from.first_name}`
    + (ctx.from.last_name ? ` last_name=${ctx.from.last_name}` : '')
    + (ctx.from.username ? ` username=${ctx.from.username}` : '')
    + ` chat.type=${ctx.chat.type}`
    + (ctx.chat.type !== 'private'
      ? (` chat.id=${ctx.chat.id}`
        + (ctx.chat.title ? ` chat.title=${ctx.chat.title}` : '')
        + (ctx.chat.username ? ` chat.username=${ctx.chat.username}` : '')
        + (ctx.chat.type ? ` chat.type=${ctx.chat.type}` : '')
      )
      : '')
}

module.exports = (ctx, next) => {
  ctx.logger = {
    info: (command, ...params) => logger.info(`${command}:`, formatParams(params), formatSrc(ctx)),
    error: (command, err, ...params) => logger.error(`${command}:`, formatParams(params), err, formatSrc(ctx))
  }
  return next()
}
