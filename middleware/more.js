const {Extra, Markup} = require('telegraf')
const delay = require('./delay')

module.exports = ctx => {
  ctx.logger.info('more-wcs')
  ctx.botanio.track('more-wcs')

  return delay(500, 1000)(ctx)
    .then(() => ctx.replyWithHTML(
      ctx.i18n.t('moreWCS'),
      Extra.webPreview(false)
        .markup(Markup
          .keyboard([Markup.button(ctx.i18n.t('keyboard.list'))])
          .oneTime(true)
          .resize(true)
        )
    ))
    .catch(err => ctx.logger.error('more', err))
}
