const {Extra} = require('telegraf')
const delay = require('./delay')
const list = require('./list')

module.exports = (ctx) => {
  ctx.logger.info('start')
  ctx.botanio.track('start')

  return delay(500, 1000)(ctx)
    .then(() => ctx.replyWithHTML(ctx.i18n.t('start'), Extra.webPreview(false)))
    .then(() => delay(300, 900)(ctx))
    .catch(err => ctx.logger.error('start', err))
    .then(() => list(ctx))
}
