const path = require('path')
const config = require('config')
const {Extra, Markup} = require('telegraf')
const delay = require('./delay')
const Request = require('../models/request')

module.exports = resource => {
  return ctx => {
    ctx.logger.info('request', resource.id)
    ctx.botanio.track(`request-${resource.id}`)

    return delay(600, 1800)(ctx)
      .then(() => ctx.replyWithPhoto(
        {source: (path.resolve(config.resourceCoverPath, resource.cover))},
        Extra
          .markup(Markup
            .keyboard([
              Markup.button(ctx.i18n.t('keyboard.list')),
              Markup.button(ctx.i18n.t('keyboard.donate'))
            ], {columns: 2})
            .oneTime(true)
            .resize(true)
          )
      ))
      .catch(err => ctx.logger.error('request', err))
      .then(() => ctx.replyWithHTML(
        ctx.i18n.t('resourceCard', resource),
        Extra
          .webPreview(false)
          .markup(Markup
            .inlineKeyboard([Markup.urlButton(ctx.i18n.t('resourceLink'), resource.url)])
          )
      ))
      .catch(err => ctx.logger.error('request', err))
      .then(() => {
        const request = new Request({from: ctx.from, resourceId: resource.id})
        return request.save()
      })
      .catch(err => ctx.logger.error('request', err))
      .then(() => {
        ctx.state.user.counter.requests++
        return ctx.state.user.save()
      })
      .catch(err => ctx.logger.error('request', err))
  }
}
