const config = require('config')
const {Extra, Markup} = require('telegraf')
const delay = require('./delay')

module.exports.paid = ctx => {

  function sendRandomSticker(ctx) {
    return ctx.telegram.getStickerSet(config.dialog.paid.setName)
      .then(set => {
        const sticker = set.stickers[Math.floor(Math.random() * set.stickers.length)]
        return ctx.replyWithSticker(sticker.file_id)
      })
  }

  ctx.logger.info('dialog-paid')
  ctx.botanio.track('dialog-paid')

  return delay(600, 1200)(ctx)
    .then(() => sendRandomSticker(ctx))
    .catch(err => ctx.logger.error('dialog-paid', err))
    .then(() => ctx.replyWithHTML(
      ctx.i18n.t('dialog.paid'),
      Extra.webPreview(false)
        .markup(Markup
          .keyboard([Markup.button(ctx.i18n.t('keyboard.list'))])
          .oneTime(true)
          .resize(true)
        )
    ))
    .catch(err => ctx.logger.error('dialog-paid', err))
    .then(() => {
      ctx.state.user.counter.paidButton++
      return ctx.state.user.save()
    })
    .catch(err => ctx.logger.error('dialog-paid', err))
}
