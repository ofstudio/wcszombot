const {Extra} = require('telegraf')
const delay = require('./delay')

module.exports = ctx => {
  ctx.logger.info('not-understand', ctx.message.text)
  ctx.botanio.track('not-understand')

  return delay(500, 1000)(ctx)
    .then(() => ctx.replyWithHTML(ctx.i18n.t('notUnderstand'), Extra.webPreview(false)))
    .then(() => delay(500, 1500)(ctx))
    .then(() => ctx.replyWithHTML(ctx.i18n.t('commands'), Extra.webPreview(false)))
    .catch(err => ctx.logger.error('not-understand', err))
}
