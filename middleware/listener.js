const Message = require('../models/message')

module.exports = (ctx, next) => {
  (new Message(ctx.message))
    .save()
    .catch(err => ctx.logger.error('listener', err))
  return next()
}
