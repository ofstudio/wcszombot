const User = require('../models/user')
const isAdmin = require('./is-admin')

module.exports = (ctx, next) => {
  return User.findOne({'telegram.id': ctx.from.id}).exec()
    .then(user => {
      if (!user) {
        user = new User({telegram: ctx.from})
      }
      user.telegram = ctx.from
      user.counter.activity++
      return user.save()
    })
    .then(user => {
      ctx.state.user = user
      ctx.state.user.isAdmin = isAdmin(ctx.from.id)
    })
    .catch(err => ctx.logger.error('user:', err, ctx.from))
    .then(() => ctx.state.user ? next() : null)
}
