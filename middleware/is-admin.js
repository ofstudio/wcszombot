const admins = (typeof process.env.ADMINS === 'string' && process.env.ADMINS.length > 0)
  ? process.env.ADMINS.split(':')
  : []

function isAdmin(id) {
  return admins.indexOf(id.toString()) >= 0
}

module.exports = isAdmin

module.exports.middleware = (ctx, next) => {
  if (isAdmin(ctx.from.id)) {
    return next()
  }
}
