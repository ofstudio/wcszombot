const {Extra, Markup} = require('telegraf')
const delay = require('./delay')

module.exports = ctx => {
  ctx.logger.info('donate')
  ctx.botanio.track('donate')

  return delay(500, 1000)(ctx)
    .then(() => ctx.replyWithHTML(
      ctx.i18n.t('donate'),
      Extra
        .webPreview(false)
        .markup(Markup
          .keyboard([Markup.button(ctx.i18n.t('keyboard.paid'))])
          .oneTime(true)
          .resize(true))
    ))
    .catch(err => ctx.logger.error('donate', err))
}
