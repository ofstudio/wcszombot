const config = require('config')
const {Extra, Markup} = require('telegraf')
const delay = require('./delay')

const list = config.resources.map(r => `/${r.id} — <b>${r.title}</b>`).join('\n')

module.exports = ctx => {
  ctx.logger.info('list')
  ctx.botanio.track('list')

  return delay(500, 1000)(ctx)
    .then(() => ctx.replyWithHTML(
      ctx.i18n.t('listHeader') + list,
      Extra.webPreview(false)
        .markup(Markup
          .keyboard([
            Markup.button(ctx.i18n.t('keyboard.commands')),
            Markup.button(ctx.i18n.t('keyboard.donate'))
          ], {columns: 2})
          .oneTime(true)
          .resize(true))
      )
    )
    .catch(err => ctx.logger.error('list', err))
}
