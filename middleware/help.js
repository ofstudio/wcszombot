const {Extra} = require('telegraf')
const delay = require('./delay')

module.exports = ctx => {
  ctx.logger.info('help')
  ctx.botanio.track('help')

  return delay(500, 1000)(ctx)
    .then(() => ctx.replyWithHTML(ctx.i18n.t('help', {commands: ctx.i18n.t('commands')}), Extra.webPreview(false)))
    .catch(err => ctx.logger.error('help', err))
}


module.exports.commands = ctx => {
  ctx.logger.info('help-commands')
  ctx.botanio.track('help-commands')

  return delay(500, 1000)(ctx)
    .then(() => ctx.replyWithHTML(ctx.i18n.t('commands'), Extra.webPreview(false)))
    .catch(err => ctx.logger.error('help-commands', err))
}
