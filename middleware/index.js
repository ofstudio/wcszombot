module.exports = {
  check: require('./check'),
  dialog: require('./dialog'),
  donate: require('./donate'),
  help: require('./help'),
  list: require('./list'),
  listener: require('./listener'),
  logger: require('./logger'),
  more: require('./more'),
  notUnderstand: require('./not-understand'),
  request: require('./request'),
  start: require('./start'),
  user: require('./user')
}
