const config = require('config')
const Scene = require('telegraf/scenes/base')
const delay = require('./delay')

const scene = new Scene('checkScene')

scene.enter(ctx => {
  ctx.scene.state.callbackUpdate = ctx.update
  getQuestion(ctx)
  return ctx.replyWithHTML(ctx.i18n.t('questionHeader'))
    .then(() => delay(700, 1500)(ctx))
    .then(() => ctx.replyWithHTML(ctx.state.question.q))
    .catch(err => ctx.logger.error('check-enter', err))
})


scene.command((ctx, next) => {
  ctx.logger.info('check-canceled', ctx.scene.state.questionId)
  ctx.botanio.track('check-canceled')

  ctx.session.check = false
  ctx.scene.leave()
  ctx.state.isCommand = true
  return next()
})


scene.on('message', (ctx, next) => {

  if (ctx.state.isCommand) {
    return next()
  }

  if (checkAnswer(ctx.scene.state.questionId, ctx.message)) {
    ctx.logger.info('check-passed', ctx.scene.state.questionId, ctx.message.text)
    ctx.botanio.track('check-passed')

    ctx.update = ctx.scene.state.callbackUpdate
    ctx.scene.leave()
    ctx.session.check = true
    return ctx.replyWithHTML(ctx.i18n.t('rightAnswer'))
      .then(() => delay(700, 1500)(ctx))
      .catch(err => ctx.logger.error('check-passed', err))
      .then(() => next())
  } else {
    ctx.logger.info('check-failed', ctx.scene.state.questionId, ctx.message.text)
    ctx.botanio.track('check-failed')

    ctx.session.check = false
    getQuestion(ctx)
    return ctx.replyWithHTML(ctx.i18n.t('wrongAnswer'))
      .then(() => delay(700, 1500)(ctx))
      .then(() => ctx.replyWithHTML(ctx.state.question.q))
      .catch(err => ctx.logger.error('check-failed', err))
  }
})


function getQuestion(ctx) {
  ctx.state.question = config.questions[Math.floor(Math.random() * config.questions.length)]
  ctx.scene.state.questionId = ctx.state.question.id
  ctx.logger.info('check-question', ctx.scene.state.questionId)
  ctx.botanio.track('check-question')
}


function checkAnswer(id, message) {
  if (!id || !message || typeof message.text !== 'string') {
    return false
  }
  return config.questions.some(question => {
    if (question.id !== id) {
      return false
    }
    return message.text.match(question.a) !== null
  })
}


module.exports = (ctx, next) => {
  if (ctx.session.check !== true) {
    return ctx.scene.enter('checkScene')
  }
  return next()
}

module.exports.scene = scene
