FROM ofstudio/node:8-alpine

# Create app directory
VOLUME /data/log
WORKDIR /app

# Install app dependencies
COPY package.json package-lock.json ./
RUN npm install --production

# Bundle app source
COPY . .

CMD ./bin/bot
